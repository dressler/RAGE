# RAGE RAngeland Grazing modEl

*NetLogo 6 Branch*
--------------------------------------------------------------------------------

## About

The RAGE model is a multi-agent simulation model that captures feedbacks between pastures, livestock and household livelihood in a common property grazing system. It implements three stylized household behavioral types (traditional, maximizer and satisficer) which are grounded in social theory and reflect empirical observations. These types can be compared regarding their long-term social-ecological consequences. The types differ in their preferences for livestock, how they value social norms concerning pasture resting and how they are influenced by the behavior of others.

Besides the evaluation of the behavioral types, the model allows to adjust a range of ecological and climatic parameters, such as rainfall average and variability, vegetation growth rates or livestock reproduction rate. The model can be evaluated across a range of social, ecological and economic outcome variables, such as average herd size, pasture biomass condition or surviving number of households.

## Version Changelog

### 2017-09-03

- Initial NetLogo 6 Version
- Includes experimental Cobb-Douglas Utility function for "MAX" behavioral type (parameter beta)
- Includes min-viable-herdsize parameter

### 2018-03-15

- Model cleanup: removed unnecessary / experimental functions

### 2018-05-10

- small bugfixes
- some BehaviorSpace experiments added

### 2018-05-26

- Vid extension uncommented (for Cluster runs)
- created with NetLogo 6.0.3

### 2018-10-26

- calculation of initial reserve biomass changed: analytical growth limit --> for MSY calculation
- added some details to the model description tab (standard parameters)
- added some BehaviorSpace experiments

### 2019-03-25

- added subdivision of yearly timesteps into smaller intervals:
    - ticks-per-year: defines the number of timesteps per year (e.g. seasons --> 4, months --> 12)
    - biomass growth is calculated once per year (first tick of the year), and is then distributed in each season (equal shares)
    - intake: now reflects seasonal intake, yearly --> intake-annual
    - reproduction happens only once per year (first tick of the year)
    - seasonal-destock?: defines whether destock of animals happens in every season or only once at the end of the year
    - livestock-feed: old function renamed to livestock-feed-and-destock --> new functions: livestock-feed, livestock-destock

### 2019-04-19

- added new BehaviorSpace experiments:
    - RAFI_TRAD_ticksPerYear_1_12
    - RAFI_MAX_ticksPerYear_1_12_nH_25-45
    - RAFI_SAT_ticksPerYear_1_12_nH_25-45

### 2019-05-02

- added seasonal-biomass-growth? parameter: biomass growth can happen all at once at the beginning of the year, or biomass is added in equal shares in each season
- corrected gini calculation (no errors livestock / destock sums are 0)
- added output_livestock_biomass function to write results of individual runs to text file (for quicker error checking of yearly vs. seasonal dynamics)
