;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RAGE RAngeland Grazing ModEl ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; part of the base model (vegetation and livestock growth) and the plotting and output functions are moved to external source files
;; you can have a look at them if you're curious but you probably won't need to modify them
__includes [ "RAGE_VegetationLivestockModel.nls" "RAGE_PlottingOutput.nls" ]

;extensions [ vid ]

globals
[
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; model parameters
  ;; variable names that are commented are defined via the interface, only noted here for completeness
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;gr1                                ; grazing pressure on green biomass
  ;gr2                                ; grazing pressure on reserve biomass
  ;w                                  ; biomass growth rate
  rue                                 ; rain use efficiency
  mg                                  ; mortality of green biomass
  ;mr                                  ; mortality of reserve biomass
  lambda                              ; growth limit of green biomass
  Rmax                                ; maximum reserve biomass
  d                                   ; density dependence of reserve biomass, d = 1/Rmax
  ;R0part                              ; initial reserve biomass as fraction of Rmax
  green-biomass-carry-over?           ; should green biomass over be carried over to the next year?
  ;rain-mean                          ; average rainfall in [mm]
  ;rain-std                           ; standard deviation of rainfall
  ;b                                  ; sheep birth rate
  intake-annual                       ; annual fodder intake of livestock
  intake                              ; sheep fodder intake per tick (depedning on ticks-per-year)
  global-rain                         ; rainfall value for all pastures when global rainfall is used
  ;number-households                  ; number of households in the system at the beginning
  number-pastures                     ; number of pastures in the system
  ;timesteps                          ; number of ticks that the simulation will be run
  ;start-on-homepatch?                ; should the households start each tick on their initial (home) patch, or their current one?
  ;resting?                           ; should resting be used?
  ;knowledge-radius                   ; knowledge radius of the households
  ;minimum-viable-herd-size           ; minimum viable herds size that households need to fulfil their livelihood
  ;behavioral-type                    ; decision model of the households
  ;satisficing-threshold              ; behavioral parameter
  ;satisficing-trials                 ; behavioral parameter
  local-first?                        ; behavioral parameter
  descriptive-norm
  use-rain-from-file?
  global-rainfall-list

  ;; other variables
  sum-livestock-total
  model-seed
  household-strategy-counts           ; list of mixed strategy counts

  vid-step
  steps-measure
]

;; household turtles
breed [ households household ]

patches-own
[
  green-biomass-init ; green biomass at the beginning of the tick, before grazing
  green-biomass ; actual available green biomass, i.e. possibly diminshed due to grazing
  reserve-biomass
  reserve-biomass-edible ; part of reserve biomass that can be consumed in the current tick
  reserve-biomass-edible-init
  rain
  is-being-grazed?
  patch-available-for-grazing? ; NH patch-available-for-grazing? = TRUE IFF is-rested? = TRUE; Todo: delete variable

  ;; variables for policy: RESTING
  consecutive-years-grazed
  consecutive-years-rested
]

households-own
[
  livestock                                   ; current herd size
  destock                                     ; current number of destocked animals
  fodder-intake                               ; fodder intake of herd during the year (used to measure fodder deficiency to see if animals need to be destocked)
  homepatch                                   ; home patch of the household

  ;; households strategy parameters
  household-behavioral-type                   ; which behavioral type does the household follow (e.g. one of TRAD, MAX, SAT)
  household-knowledge-radius                  ; radius of perception of pastures around own location
  household-local-neighborhood

  household-intrinsic-preference              ; intrinsic preference for pasture resting
  household-social-susceptibility             ; susceptibility to behavior of other households
  household-satisficing-threshold             ; satisficing threshold for herd size
  current-household-satisficing-threshold     ; gets updated if current herd size is lower than the actual satisficing threshold (e.g. due to destocking)
  household-effective-propensity              ; effective propensity of the household for resting / not resting, given its preferences and the descriptive norm
  household-resting-behavior                  ; 1 - household abides to resting rule, 0 - household does not abide to resting rule

  household-satisficing-trials                ; for SAT: number of pastures that the household evaluates when following type SAT (i.e. defines its cognitive limitation)
  household-local-first                       ; for SAT: should the household search in it's local neighborhood first or is distance irrelevant when evaluating patches?

  ;; variables to keep track of livestock / destock over time
  livestock-total
  destock-total

  ;; variables to keep track of livestock feeding over course of the year
  fodder-needed-annual                        ; total annual fodder need of livestock, based on herd size at beginning of the year (after reproduction)
  fodder-already-eaten                        ; fodder intake of herd that as already taken place over the year
]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MODEL SETUP ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to setup
  clear-ticks
  clear-turtles
  clear-patches
  clear-drawing
  clear-all-plots
  clear-output

  ;; set the model random seed
  ifelse ( fixed-seed? )
  [
    set model-seed 12345
  ]
  [
    set model-seed new-seed
  ]
  random-seed model-seed

  ;; set model parameter values
  set timesteps years * ticks-per-year
  set rue 0.002
  set mg 0.0
  ;set mr 0.1
  set Rmax 150000
  set d 1 / Rmax
  ;set R0part 0.6
  set lambda 2.5
  set intake-annual 640
  set intake intake-annual / ticks-per-year ; LIVESTOCK EATS ONLY PART OF THEIR YEARLY CONSUMPTION ON EACH TICK OF THE YEAR
  set number-pastures 100
  set local-first? true
  set green-biomass-carry-over? true

  set use-rain-from-file? false

  if ( use-rain-from-file? )
  [
    ifelse ( file-exists? "rainfall_list.txt" )
    [
      file-open "rainfall_list.txt"
      set global-rainfall-list file-read
      file-close
    ]
    [
      user-message ( "No rainfall file found, create a rainfall file with generate-rain-list first!" )
    ]
  ]

  ;; now create a number of pastures and initialize them
  ask patches
  [
    set rain rainfall
    set reserve-biomass Rmax * ( 1 - mr / ( w * lambda ) ) * R0part ; relative to carrying capacity ;R0part * Rmax
    set reserve-biomass-edible reserve-biomass * gr2
    set green-biomass-init  ( Rmax * rain-mean * rue * (lambda * w - mr)) / (2 * lambda * w)  ;lambda * reserve-biomass
    set green-biomass  ( Rmax * rain-mean * rue * (lambda * w - mr)) / (2 * lambda * w) ;lambda * reserve-biomass

    ;; per default, all patches are available for grazing
    set patch-available-for-grazing? true
    set is-being-grazed? false

    ;; variables for resting policy
    set consecutive-years-grazed 0
    ;NH set consecutive-years-rested 0

    ;; layout and colors
    set pcolor scale-color green (reserve-biomass-edible + green-biomass) ( 73000 + gr2 * Rmax ) 0
    set plabel-color white
    set plabel ""
  ]

  ;; create a number of households and initialize them with an empty livestock array
  ;; households are initally placed on a random pasture where no other households are present

  if (number-households > 100)
  [
    user-message "Not more than 100 households are allowed - households are automatically set to 100"
    set number-households 100
  ]

  ask n-of number-households patches
  [
    sprout-households 1
    [
      set homepatch patch-here
      set shape "house4"
      set livestock ( Rmax * rain-mean * rue * (lambda * w - mr)) / (2 * lambda * w) / intake ;satisficing-threshold ; 10
      set destock 0
      set pen-size 1
      set color red
      set label-color white
      ; behavioral types will be set up in seperate procedure
      set household-behavioral-type "none"
      ; adjust the size of the household shape according to their current number of livestock
      set size max list 0.25 (livestock / 120)
    ]
  ]

  ; for behavior space
  if homog-behav-types?
  [
    ask households
    [
      set household-behavioral-type behavioral-type
    ]
  ]

  ;; setup household relocation strategies
  setup-household-behavioral-types

  set descriptive-norm ( count households with [ patch-available-for-grazing? = true ] ) / ( count households )

  reset-ticks

  output_livestock_biomass

;  if vid:recorder-status = "recording"
;  [
;    vid:record-interface
;    set vid-step 0
;  ]
end

to setup-household-behavioral-types
  ;; function to setup individual strategies of households

  ;; determine if we're running a simulation with homogeneous or mixed behavioral types
  if not homog-behav-types?
  [
    ; heterogeneous types: numbers set via sliders in interface
    set household-strategy-counts ( list number-random number-MAX number-SAT number-TRAD )
    let strategy-list ( list "Random" "MAX" "SAT" "TRAD" )
    let curr-strategy 0

    foreach household-strategy-counts
    [ ?1 ->
      ask n-of ?1 households with [ household-behavioral-type = "none" ]
      [
        set household-behavioral-type item curr-strategy strategy-list
      ]
      set curr-strategy curr-strategy + 1
    ]
  ]

  ask households
  [
    ;NH all households have the same knowledge-radius
    set household-knowledge-radius knowledge-radius ;NH
    set household-local-neighborhood moore-neighborhood household-knowledge-radius ;NH

    if ( household-behavioral-type = "Random" )
    [
      set color yellow
      ; no parameters to set for this strategies
    ]
    if ( household-behavioral-type = "MAX" )
    [
      set color red
      ; MAX type - no preference for resting, no social susceptibility
      ;            satisficing threshold quasi infinity
      set household-intrinsic-preference 0.0
      set household-social-susceptibility 0.0
      set household-satisficing-threshold 9999
      set household-satisficing-trials 9999
      set household-local-first local-first?
      set household-resting-behavior 0
      ; no parameters to set for these strategies
    ]
    if ( household-behavioral-type = "SAT" )
    [
      set color blue
      ; SAT type - no preference for resting, no social susceptibility
      ;            set satisficing threshold accoring to sliders
      ;set household-intrinsic-preference 0.0
      ;set household-social-susceptibility 0.0
      set household-intrinsic-preference intrinsic-preference
      set household-social-susceptibility social-susceptibility
      set household-satisficing-threshold satisficing-threshold
      set household-satisficing-trials satisficing-trials
      set household-local-first local-first?
      set household-resting-behavior 0
    ]
    if ( household-behavioral-type = "TRAD" )
    [
      ; TRAD type - intrinscic preference and social susceptibility according to sliders
      ;             satisficing threshold quasi infinity
      set household-intrinsic-preference intrinsic-preference
      set household-social-susceptibility social-susceptibility
      set household-satisficing-threshold 9999
      set household-satisficing-trials 9999
      set household-local-first local-first?
      set household-resting-behavior 0
      set color scale-color violet household-intrinsic-preference 0 1
    ]
  ]
end


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GO FUNCTION ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to go
  ;; check first if we have reached the end of the simulation
  if ( ticks > timesteps - 1)  [ stop ]

  ;; I'm using tick-advance instead of tick to avoid the automatic updating of the plots
  tick-advance 1

  if ( global-rain? )
  [
    ifelse ( use-rain-from-file? )
    [
      set global-rain item ticks global-rainfall-list
    ]
    [
      ;; calculate one global rainfall value if option global-rain? is checked
      set global-rain rainfall
    ]
  ]

  if ( resting? and ticks mod ticks-per-year = 0 )
  [
    ;; update resting state of patches (only once per year, not every step of the year)
    update-resting-patches
  ]

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; patches
  ifelse ( ticks mod ticks-per-year = 0 )
  [
    ask patches
    [
      ;; rainfall
      ifelse ( global-rain? )
      [
        set rain global-rain
      ]
      [
        set rain rainfall
      ]
      ;; green biomass growth
      set green-biomass-init ( green-biomass-growth green-biomass rain reserve-biomass )
      ; calculate the amount of reserve biomass that can be consumed in this tick
      set reserve-biomass-edible-init reserve-biomass * gr2
      ;; new year, so patches have not been grazed yet
      set is-being-grazed? false
      ifelse ( seasonal-biomass-growth? )
      [
        set green-biomass green-biomass-init / ticks-per-year
        set reserve-biomass-edible reserve-biomass-edible-init / ticks-per-year
      ]
      [
        set green-biomass green-biomass-init
        set reserve-biomass-edible reserve-biomass-edible-init
      ]

      if ( any? households with [ household-behavioral-type = "TRAD" ] )
      [
        ;set descriptive-norm mean [ household-intrinsic-preference ] of households

      ]
    ]
  ]
  [
    if ( seasonal-biomass-growth? )
    [
      ask patches
      [
        set green-biomass green-biomass + ( green-biomass-init / ticks-per-year ) ;; every month / sub-tick we add a share of the annual biomass production
        set reserve-biomass-edible reserve-biomass-edible + reserve-biomass-edible-init / ticks-per-year
      ]
    ]
  ]
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; households
  ask households
  [
    ;; check if any households lost all their livestock in the previous step (livestock = 0)
    if ( hh-exit? = false and livestock <= 0 )
    [
      ; let them check the capacity of their current patch, and let them return with half of what the patch could sustain
      let current-patch-capacity green-biomass / intake-annual
      set livestock current-patch-capacity / 2
      ;print ( word who ": new herd with size = " livestock )
    ]
    ;; livestock growth - REPRODUCTION ONLY ONCE PER YEAR, ACCORDING TO TICKS-PER-YEAR VALUE
    if ( ticks mod ticks-per-year = 0 ) ;round ( ticks-per-year / 2 ) )
    [
      set livestock livestock-growth livestock
      set destock 0
      if household-behavioral-type = "SAT"
      [
        ; if household is of type SAT, he will destock to his satisficing threshold first before relocation
        destock-SAT
      ]
      set fodder-needed-annual intake-annual * livestock
      set fodder-intake 0
    ]
    ;; move back to homepatch, if start-on-homepatch is set to true
    if ( start-on-homepatch? ) [ move-to homepatch ]
    ;; relocate the households according to the choosen relocation strategy
    relocate-household
    ;; let livestock feed
    ifelse seasonal-destock?
    [ livestock-feed-with-destock ]
    [ livestock-feed ]
    ;; potentially destock livestock - DESTOCKING HAPPENS AT THE END OF THE YEAR ONLY
    if (ticks mod ticks-per-year = ( ticks-per-year - 1 ) )
    [
      if ( not seasonal-destock? ) [ livestock-destock ]
    ]
    ;; check if households have zero livestock and if so, let them die
    if (livestock <= 0 and hh-exit? )
    [
      ;print ( word who ": died, probably because of hunger > reenters with 1 sheep" )
      die
    ]
;    if vid:recorder-status = "recording"
;    [
;      ifelse ( vid-step > 10 )
;      [
;        vid:record-interface
;        set vid-step 0
;      ]
;      [
;        set vid-step vid-step + 1
;      ]
;    ]
  ]
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; patches
  if ( ticks mod ticks-per-year = ( ticks-per-year - 1) ) ;; 0 OR ( ticks-per-year - 1) ??? --> should be the latter, reserve biomass grows only at the end of the year...
  [
    ask patches
    [
      ;; reserve biomass growth
      set reserve-biomass ( reserve-biomass-growth green-biomass-init green-biomass reserve-biomass
        reserve-biomass-edible )
    ]
  ]

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; check whether resting is activated and update patches accordingly
;  if resting?
;  [
;    update-resting-patches
;  ]
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; update all plots & view, total livestock sum
  update-color-of-patches
  update-plots
  set sum-livestock-total sum-livestock-total + sum [ livestock ] of households
  set descriptive-norm ifelse-value ( count households > 0 )
  [
    ;; different calculation but yields the same result
    ;; ( count households with [ patch-available-for-grazing? = true ] ) / ( count households )
    mean [ household-resting-behavior ] of households
  ]
  [
    1
  ]
  output_livestock_biomass

  ;if vid:recorder-status = "recording" [ vid:record-interface ]
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HOUSEHOLD MOVEMENT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to relocate-household
  ; STEP 1:
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; first calculate which patches are available for use based on the DESCRIPTIVE NORM, the own intrinsic preference and the social susceptibility
  let relevant-patches patches
  ; calculate effective propensity
  set household-effective-propensity household-social-susceptibility * descriptive-norm + ( 1 - household-social-susceptibility ) * household-intrinsic-preference
  ; determine set of patches
  ifelse random-float 1 < household-effective-propensity ;< household-intrinsic-preference
  [
    ; only rested patches are available (within the local neighborhood, as defined by knowledge-radius)
    set relevant-patches patches at-points household-local-neighborhood with [ patch-available-for-grazing? ]
  ]
  [
    ; all patches are available (within the local neighborhood, as defined by knowledge-radius)
    set relevant-patches patches at-points household-local-neighborhood
  ]
  ; update color of household to reflect it's effective propensity (for TRAD households)
  if ( household-behavioral-type = "TRAD" ) [ set color scale-color violet household-effective-propensity 0 1 ]
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ; STEP 2:
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ifelse (count relevant-patches != 0)
  [
    ;; now that we have determined a set of relevant patches, we need to select one specific patch
    ;; SAT ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    if ( household-behavioral-type = "SAT" )
    [
      ; household is a satisficer, therefore we use satisficing as selection method for the patch
      let patch-found false
      let number-trials 0
      let current-patch patch-here
      let current-best-patch nobody

      ;; update the household satisficing threshold if he currently has less livestock than the global threshold
      set current-household-satisficing-threshold min ( list household-satisficing-threshold livestock )

      ;; first check if the household will be satisfied on his current-patch
      if ( ( [ green-biomass + reserve-biomass-edible ] of current-patch ) / intake >= current-household-satisficing-threshold and local-first? )
      [
        set patch-found true
        update-household-behavior
      ]

      ;; b) by local-first?: sort patches by distance or randomize patch order
      let relevant-patches-ordered ifelse-value local-first? [ sort-on [ distance myself ] relevant-patches ] [ shuffle sort relevant-patches ]
      ;; c) by the number of satisficing-trials: no more than #satisficing-trials patches will be evaluated
      let relevant-patches-ordered-sub sublist relevant-patches-ordered 0 min ( list satisficing-trials length relevant-patches-ordered )

      while [ patch-found = false and number-trials < length relevant-patches-ordered-sub ]
      [
        ;; loop until the end of the patch list is reached or a satisficing patch is found
        set current-patch item number-trials relevant-patches-ordered-sub
        ifelse ( ( [ green-biomass + reserve-biomass-edible ] of current-patch ) / intake >= current-household-satisficing-threshold )
        [
          move-to current-patch
          set patch-found true
          ; check whether selected patch is suitably rested (i.e. available for grazing) and update hh behavior accordingly
          update-household-behavior
        ]
        [
          ;; if the patch doesn't meet the satisficing threshold, save it as current-best-patch if it has higher
          ;; capacity than all previous evaluated patches
          set number-trials number-trials + 1
          ifelse ( current-best-patch = nobody )
          [
            set current-best-patch current-patch
          ]
          [
            set current-best-patch max-one-of ( patch-set current-patch current-best-patch ) [ green-biomass + reserve-biomass-edible ]
          ]
        ]
      ]

      if ( patch-found = false )
      [
        ;; if no satisficing patch is found, move to the best patch that has been found so far
        ifelse current-best-patch != nobody
        [
          move-to current-best-patch
          update-household-behavior
        ]
        [
          set livestock 0
        ]
      ]
    ]
    ;; TRAD / MAX ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    if ( household-behavioral-type = "MAX" or household-behavioral-type = "TRAD" )
    [
      ; households are either TRAD or MAX type, therefore we use maximizing as selection method
      ; move to the patch that provides the highest amount of biomass
      move-to max-one-of relevant-patches [ reserve-biomass-edible + green-biomass ]
      update-household-behavior
    ]
    ;; Random ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
    if ( household-behavioral-type = "Random" )
    [
      ;move randomly to one patch
      move-to one-of relevant-patches
      update-household-behavior
    ]
  ]
  [
    ; no relevant set of patches could be determined
    ; check if TRAD type
    ; if ( household-behavioral-type = "TRAD" ) <-- think about new "resting violation" rule
    ;, household dies
    set livestock 0
    ; print ( word who ": died, set of relevant patches: " relevant-patches )
    ;die
  ]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LIVESTOCK FEEDING ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to livestock-feed-with-destock
  ;; calculate livestock fodder needed
  let fodder-needed intake * livestock
  let min-green-biomass 0 ;2 * 640

  ;; based on the caluclated fodder, there can be three cases:
  ;; case 1: destocking: yes, green-biomass: all consumed, edible reserve biomass: all consumed
  ifelse fodder-needed > (reserve-biomass-edible + ( green-biomass - min-green-biomass ))
  [
    ;; not enough biomass available, all biomass will be consumed and part of the livestock will be destocked
    ; let current-destock ceiling ( ( fodder-needed - (reserve-biomass-edible + ( green-biomass - min-green-biomass )) ) / intake )
    let current-destock ( fodder-needed - (reserve-biomass-edible + ( green-biomass - min-green-biomass )) ) / intake
    set livestock ( livestock - current-destock )
    ;log output
    if ( livestock <= 0 )
    [
      ;print ( word who ": livestock set to zero because not enough biomass available, livestock = " livestock )
      set livestock 0
    ]
    set destock destock + current-destock
    ;; no green biomass and no edible reserve biomass is left
    set green-biomass min-green-biomass ;0
    set reserve-biomass-edible 0
  ]
  [
    ;; case 2: destocking: no, green-biomass: all consumed, edible reserve biomass: partially consumed
    ifelse fodder-needed > ( green-biomass - min-green-biomass )
    [
      ;; all green biomass is consumed and some part of edible reserve biomass as well,
      ;; but livestock does not need to be destocked
      set reserve-biomass-edible ( reserve-biomass-edible - ( fodder-needed - ( green-biomass - min-green-biomass ) ) )
      set green-biomass min-green-biomass ;0
    ]
    [
      ;; case 3: destocking: no, green-biomass: partially consumed, edible reserve biomass: not consumed
      set green-biomass ( green-biomass - fodder-needed )
    ]
  ]

  ;; keep track of total livestock and destock over time
  set livestock-total livestock-total + livestock
  set destock-total destock-total + destock

  ;; as long as the livestock is > 0 (i.e. not all animals have been destocked), the patch has been grazed in this year
  ifelse livestock > 0
  [
    set is-being-grazed? true
  ]
  [
    set is-being-grazed? false
  ]
end

to livestock-feed
  ;; calculate livestock fodder needed
  ;; intake = intake per season! (yearly = intake-annual)
  let fodder-needed intake * livestock
  let min-green-biomass 0 ;2 * 640

  ;; based on the caluclated fodder, there can be three cases:
  ;; case 1: destocking: yes, green-biomass: all consumed, edible reserve biomass: all consumed
  ifelse fodder-needed > (reserve-biomass-edible + ( green-biomass - min-green-biomass ))
  [
    ;; not enough biomass available in current step (season), all biomass will be consumed
    set fodder-intake fodder-intake + reserve-biomass-edible + ( green-biomass - min-green-biomass )
    ;print ( word "tick " ticks " case 1: livestock = " livestock ", available biomass = " ( reserve-biomass-edible + ( green-biomass - min-green-biomass ) ) ", fodder-needed = " fodder-needed )
    ;let current-destock ceiling ( ( fodder-needed - (reserve-biomass-edible + ( green-biomass - min-green-biomass )) ) / intake )
    ;set livestock ( livestock - current-destock )
    ;set destock destock + current-destock
    ;; no green biomass and no edible reserve biomass is left
    set green-biomass min-green-biomass ;0
    set reserve-biomass-edible 0
  ]
  [
    ;; case 2: destocking: no, green-biomass: all consumed, edible reserve biomass: partially consumed
    ifelse fodder-needed > ( green-biomass - min-green-biomass )
    [
      ;; all green biomass is consumed and some part of edible reserve biomass as well,
      ;; but livestock does not need to be destocked
      ;print ( word "tick " ticks " case 2: livestock = " livestock ", available biomass = " ( reserve-biomass-edible + ( green-biomass - min-green-biomass ) ) ", fodder-needed = " fodder-needed )
      set fodder-intake fodder-intake + fodder-needed
      set reserve-biomass-edible ( reserve-biomass-edible - ( fodder-needed - ( green-biomass - min-green-biomass ) ) )
      set green-biomass min-green-biomass ;0
    ]
    [
      ;; case 3: destocking: no, green-biomass: partially consumed, edible reserve biomass: not consumed
      ;print ( word "tick " ticks " case 3: livestock = " livestock ", available biomass = " ( reserve-biomass-edible + ( green-biomass - min-green-biomass ) ) ", fodder-needed = " fodder-needed )
      set fodder-intake fodder-intake + fodder-needed
      set green-biomass ( green-biomass - fodder-needed )
    ]
  ]

  ;; keep track of total livestock and destock over time
  ;set livestock-total livestock-total + livestock
  ;set destock-total destock-total + destock

  ;; as long as the livestock is > 0 (i.e. not all animals have been destocked), the patch has been grazed in this year
  ifelse livestock > 0
  [
    set is-being-grazed? true
  ]
  [
    set is-being-grazed? false
  ]
end

to livestock-destock
  ;; check if hh needs to destock his herd, based on how much fodder intake he has had during the year
  if ( fodder-intake < fodder-needed-annual )
  [
    ;; less fodder has been consumed by the livestock than was needed, so destocking has to take place
    ;let current-destock ceiling ( ( fodder-needed-annual - fodder-intake ) / intake-annual )
     let current-destock ( fodder-needed-annual - fodder-intake ) / intake-annual
    ;print ( word who ": fodder-needed-annual = " fodder-needed-annual ", fodder-intake = " fodder-intake ", livestock = " livestock ", destock = " current-destock )
    set livestock ( livestock - current-destock )
    if ( livestock <= 0 )
    [
      ;print ( word who ": livestock set to zero because not enough biomass available, livestock = " livestock )
      set livestock 0
    ]
    set destock destock + current-destock
  ]

  ;; keep track of total livestock and destock over time
  set livestock-total livestock-total + livestock
  set destock-total destock-total + destock
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

to destock-SAT
  ;; when strategy "satisficing-2" is chosen, households will stock no more than their satisficing threshold
  ;; check whether herd size > satisficing-threshold and destock animals, if necessary
  let current-destock-sat max ( list 0 (livestock - household-satisficing-threshold ) )
  set livestock ( livestock - current-destock-sat )
  set destock destock + current-destock-sat
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; POLICY INTERVENTIONS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; RESTING ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to update-resting-patches
  ;; check all rested patches in this year
  ask patches with [ reserve-biomass < resting-threshold * Rmax ]
  [
    set patch-available-for-grazing? false
    set plabel "R"
    set plabel-color red
  ]
  ask patches with [ reserve-biomass >= resting-threshold * Rmax ]
  [
    set patch-available-for-grazing? true
    set plabel "G"
    set plabel-color white
  ]
end

to update-household-behavior
  ; check whether selected patch is suitably rested (i.e. available for grazing) and update hh behavior accordingly
  ifelse ( patch-available-for-grazing? )
  [
    set household-resting-behavior 1
  ]
  [
    set household-resting-behavior 0
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to output_livestock_biomass
  ifelse ( ticks = 0)
  [
    carefully
    [
      file-delete ( word "RAGE_seasonalDestock_" seasonal-destock? "_seasonalBiomassGrowth_" seasonal-biomass-growth? "_ticks_per_year_" ticks-per-year ".txt" )
    ] []
    file-open ( word "RAGE_seasonalDestock_" seasonal-destock? "_seasonalBiomassGrowth_" seasonal-biomass-growth? "_ticks_per_year_" ticks-per-year ".txt" )
    file-print ( word "step,seasonal_destock,seasonal_biomass_growth,ticks_per_year,livestock_mean,livestock_sum,green_biomass,green_biomass_over,reserve_biomass,rain" )
  ]
  [
    file-print ( word ( ticks / ticks-per-year ) "," seasonal-destock? "," seasonal-biomass-growth? "," ticks-per-year "," mean [ livestock ] of households "," sum [ livestock ] of households "," mean-green-biomass "," mean-green-biomass-over "," mean-reserve-biomass "," mean [ rain ] of patches )
  ]
  if ( ticks = timesteps )
  [
    file-close
  ]
end

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
@#$#@#$#@
GRAPHICS-WINDOW
405
10
682
288
-1
-1
26.9
1
10
1
1
1
0
1
1
1
-4
5
-4
5
0
0
1
ticks
30.0

BUTTON
405
290
490
323
NIL
setup
NIL
1
T
OBSERVER
NIL
P
NIL
NIL
1

BUTTON
595
290
680
323
NIL
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

PLOT
695
145
1430
280
green biomass [average +/- sd]
timestep
biomass
0.0
10.0
0.0
10.0
false
false
"" "set-plot-x-range 0 max ( list ticks 10 )\nset-plot-y-range 0 ceiling max ( list plot-y-max ( mean [ green-biomass ] of patches + standard-deviation [ green-biomass ] of patches ) )"
PENS
"green-biomass" 1.0 0 -13840069 true "" "plotxy ticks mean [ green-biomass ] of patches"
"pen-2" 1.0 0 -5509967 true "" "plotxy ticks ( mean [ green-biomass ] of patches - downside-sd [ green-biomass ] of patches )"
"pen-3" 1.0 0 -5509967 true "" "plotxy ticks ( mean [ green-biomass ] of patches + upside-sd [ green-biomass ] of patches )"

PLOT
695
420
940
540
rainfall [average +/- sd]
timestep
rainfall
0.0
10.0
0.0
10.0
true
false
"" "set-plot-x-range 0 max ( list ticks 10 )\nset-plot-y-range 0 ceiling max ( list plot-y-max ( mean [ rain ] of patches + standard-deviation [ rain ] of patches ) )"
PENS
"mean rainfall" 1.0 0 -13345367 true "" "plotxy ticks mean [ rain ] of patches"
"rainfall-sd" 1.0 0 -5325092 true "" "plotxy ticks ( mean [ rain ] of patches - standard-deviation [ rain ] of patches )"
"rainfall+sd" 1.0 0 -5325092 true "" "plotxy ticks ( mean [ rain ] of patches + standard-deviation [ rain ] of patches )"

BUTTON
505
290
590
323
step
go
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

SLIDER
130
70
285
103
number-households
number-households
1
100
90.0
1
1
NIL
HORIZONTAL

PLOT
695
10
1430
145
livestock [average +/- sd]
timestep
livestock
0.0
10.0
0.0
10.0
false
false
"" "set-plot-x-range 0 max ( list ticks 10 )\nset-plot-y-range 0 ceiling max ( list plot-y-max ( mean [ livestock ] of households + upside-sd [ livestock ] of households ) )"
PENS
"livestock" 1.0 0 -2674135 true "" "if count households > 0 [ plotxy ticks mean [ livestock ] of households ]"
"livestock-sd" 1.0 0 -1604481 false "" "if count households > 1 [ plotxy ticks ( mean [ livestock ] of households - downside-sd [ livestock ] of households ) ]"
"livestock+sd" 1.0 0 -1604481 false "" "if count households > 1 [ plotxy ticks ( mean [ livestock ] of households + upside-sd [ livestock ] of households ) ]"
"years" 1.0 0 -7500403 true "" "ifelse ( ticks-per-year > 1 and ticks mod ticks-per-year = 0 ) \n[\n  plotxy ticks 0\n  plotxy ticks 1000\n  plotxy ticks 0\n]\n[\n  plotxy ticks 0\n]"

PLOT
695
280
1430
415
reserve biomass [average +/- sd]
timestep
biomass
0.0
10.0
0.0
10.0
true
false
"" "set-plot-x-range 0 max ( list ticks 10 )\nset-plot-y-range 0 ceiling max ( list plot-y-max ( mean [ reserve-biomass ] of patches + standard-deviation [ reserve-biomass ] of patches ) )"
PENS
"default" 1.0 0 -14835848 true "" "plotxy ticks mean [ reserve-biomass ] of patches"
"pen-1" 1.0 0 -5908279 true "" "plotxy ticks ( mean [ reserve-biomass ] of patches - downside-sd [ reserve-biomass ] of patches )"
"pen-2" 1.0 0 -5908279 true "" "plotxy ticks ( mean [ reserve-biomass ] of patches + upside-sd [ reserve-biomass ] of patches )"
"carryingCap" 1.0 0 -7500403 true "" "plotxy ticks Rmax * ( 1 - mr / ( w * lambda ) )"
"halfCarryingCap" 1.0 0 -7500403 true "" "plotxy ticks Rmax * ( 1 - mr / ( w * lambda ) ) / 2"

INPUTBOX
130
195
205
255
w
0.8
1
0
Number

PLOT
1185
420
1430
540
current livestock [each household]
household
livestock
0.0
10.0
0.0
10.0
false
false
"" ""
PENS
"livestock-current" 1.0 1 -2674135 true "" "plot-livestock-per-household"

INPUTBOX
210
195
290
255
gr1
0.5
1
0
Number

INPUTBOX
295
195
375
255
gr2
0.1
1
0
Number

INPUTBOX
290
80
375
140
b
0.1
1
0
Number

TEXTBOX
130
150
310
168
Vegetation parameters
13
0.0
1

TEXTBOX
130
45
355
63
Household & livestock parameters
13
0.0
1

CHOOSER
5
290
165
335
behavioral-type
behavioral-type
"MAX" "SAT" "TRAD" "Random"
0

PLOT
695
540
940
660
surviving households [ livestock > 0 ]
timestep
household count
0.0
10.0
0.0
10.0
true
false
"" "set-plot-x-range 0 timesteps\nset-plot-y-range 0 100"
PENS
"households" 1.0 0 -16777216 true "" "plot count households with [ livestock > 0 ]"
"pen-1" 1.0 0 -2674135 true "" "plot count households with [ livestock > 0 and household-behavioral-type = \"MAX\" ]"
"pen-2" 1.0 0 -8630108 true "" "plot count households with [ livestock > 0 and household-behavioral-type = \"TRAD\" ]"
"pen-3" 1.0 0 -13345367 true "" "plot count households with [ livestock > 0 and household-behavioral-type = \"SAT\" ]"

TEXTBOX
290
65
370
83
sheep birth rate
11
0.0
1

TEXTBOX
130
165
210
195
biomass growth rate
11
0.0
1

TEXTBOX
210
165
295
195
green biomass grazing pressure
11
0.0
1

TEXTBOX
295
165
380
195
reserve biomass grazing pressure
11
0.0
1

SWITCH
5
160
120
193
global-rain?
global-rain?
0
1
-1000

TEXTBOX
5
140
155
158
Rainfall parameters
13
0.0
1

INPUTBOX
5
195
65
255
rain-mean
200.0
1
0
Number

INPUTBOX
70
195
120
255
rain-std
100.0
1
0
Number

TEXTBOX
5
15
285
40
RAGE RAngeland Grazing ModEl
18
0.0
1

SLIDER
5
410
165
443
satisficing-threshold
satisficing-threshold
0
100
50.0
1
1
NIL
HORIZONTAL

SLIDER
175
485
390
518
satisficing-trials
satisficing-trials
0
100
10.0
1
1
NIL
HORIZONTAL

SLIDER
5
485
165
518
knowledge-radius
knowledge-radius
0
5
5.0
1
1
patches
HORIZONTAL

PLOT
1430
420
1590
570
Lorenz Curve
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"lorenz" 1.0 0 -16777216 true "" "plot-lorenz"
"pen-1" 1.0 0 -7500403 true "" "plotxy 0 0\nplotxy number-households 1"

MONITOR
1430
615
1525
660
Total livestock
total-livestock / ( ticks / ticks-per-year )
2
1
11

MONITOR
1430
570
1590
615
Current mean livestock
mean-livestock
2
1
11

SWITCH
130
105
285
138
start-on-homepatch?
start-on-homepatch?
0
1
-1000

TEXTBOX
5
450
155
468
Knowledge of pastures
13
0.0
1

TEXTBOX
5
265
205
283
Behavioral types
16
0.0
1

MONITOR
1525
615
1590
660
Gini coef
gini-livestock
3
1
11

SWITCH
400
460
530
493
resting?
resting?
1
1
-1000

TEXTBOX
400
435
530
455
Pasture resting
16
0.0
1

INPUTBOX
5
70
70
130
timesteps
200.0
1
0
Number

TEXTBOX
5
45
110
63
Simulation length
13
0.0
1

SLIDER
285
375
390
408
number-random
number-random
0
number-households - number-MAX - number-SAT - number-TRAD
0.0
1
1
NIL
HORIZONTAL

SLIDER
175
340
280
373
number-MAX
number-MAX
0
number-households - number-random - number-SAT - number-TRAD
0.0
1
1
NIL
HORIZONTAL

SLIDER
175
375
280
408
number-SAT
number-SAT
0
number-households - number-MAX - number-random - number-TRAD
0.0
1
1
NIL
HORIZONTAL

SLIDER
285
340
390
373
number-TRAD
number-TRAD
0
number-households - number-MAX - number-SAT - number-random
0.0
1
1
NIL
HORIZONTAL

PLOT
940
420
1185
540
Descriptive norm
NIL
NIL
0.0
10.0
0.0
1.0
true
false
"" "set-plot-x-range 0 timesteps\nset-plot-y-range 0 1"
PENS
"default" 1.0 0 -16777216 true "" "plot descriptive-norm"

SLIDER
535
460
680
493
resting-threshold
resting-threshold
0
1
0.84
0.01
1
NIL
HORIZONTAL

SWITCH
175
305
390
338
homog-behav-types?
homog-behav-types?
0
1
-1000

SLIDER
5
340
165
373
intrinsic-preference
intrinsic-preference
0
1
0.0
0.01
1
NIL
HORIZONTAL

SLIDER
5
375
165
408
social-susceptibility
social-susceptibility
0
1
0.0
0.01
1
NIL
HORIZONTAL

TEXTBOX
175
275
390
301
For mixed behavioral types, set to \"off\" and select numbers below
11
0.0
1

TEXTBOX
5
470
155
488
relevant for all types:
11
0.0
1

TEXTBOX
175
470
325
488
relevant only for SAT type:
11
0.0
1

PLOT
695
665
1430
815
livestock sum
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "set-plot-x-range 0 max ( list ticks 10 )\nset-plot-y-range 0 ceiling max ( list plot-y-max ( sum [ livestock ] of households ) )"
PENS
"default" 1.0 0 -16777216 true "" "if count households > 0 [ plotxy ticks sum [ livestock ] of households ]"

INPUTBOX
405
335
475
395
mr
0.1
1
0
Number

PLOT
940
540
1185
660
hh with livestock = 0
NIL
NIL
0.0
10.0
0.0
1.0
false
false
"" "set-plot-x-range 0 max ( list ticks 10 )\nset-plot-y-range 0 ceiling max ( list plot-y-max ( count households with [livestock = 0] ) )"
PENS
"default" 1.0 1 -16777216 true "" "plot count households with [livestock = 0]"

SWITCH
285
415
390
448
hh-exit?
hh-exit?
1
1
-1000

INPUTBOX
480
335
530
395
R0part
0.5
1
0
Number

PLOT
1185
540
1430
660
current destock [each household]
household
livestock
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"destock-current" 1.0 1 -5298144 true "" "plot-destock-per-household"

INPUTBOX
535
335
615
395
ticks-per-year
1.0
1
0
Number

SWITCH
405
400
562
433
seasonal-destock?
seasonal-destock?
0
1
-1000

MONITOR
370
555
617
600
NIL
count patches with [ reserve-biomass < 0 ]
17
1
11

MONITOR
370
605
607
650
NIL
count patches with [ green-biomass < 0 ]
17
1
11

MONITOR
370
655
592
700
NIL
count households with [ livestock < 0 ]
17
1
11

INPUTBOX
75
70
125
130
years
200.0
1
0
Number

SWITCH
565
400
680
433
fixed-seed?
fixed-seed?
1
1
-1000

SWITCH
400
505
602
538
seasonal-biomass-growth?
seasonal-biomass-growth?
0
1
-1000

@#$#@#$#@
# RAGE RAngeland Grazing ModEl

## WHAT IS IT?

In many dryland regions, traditional pastoral land use strategies are subject to change. Drivers such as demographic change, but also socio-economic change (liberalization of markets, new income options) may lead to an adjustment of livelihood strategies and behavior of pastoral households.

The RAGE model is a *multi-agent simulation model* that captures *feedbacks between pastures, livestock and household livelihood in a common property grazing system*. It implements three stylized *household behavioral types* (traditional *TRAD*, maximizer *MAX* and satisficer *SAT*) which are grounded in social theory and reflect empirical observations. These types can be compared regarding their long-term social-ecological consequences. The types differ in their preferences for livestock, how they value social norms concerning pasture resting and how they are influenced by the behavior of others.

Besides the evaluation of the behavioral types, the model allows to adjust a range of ecological and climatic parameters, such as rainfall average and variability, vegetation growth rates or livestock reproduction rate. The model can be evaluated across a range of social, ecological and economic outcome variables, such as average herd size, pasture biomass condition or surviving number of households.

## HOW IT WORKS

### Yearly sequence of processes

The model consists of three main components: _**Rainfall**_, _**Vegetation**_, and _**Pastoralist households and livestock**_. Those components are linked by the following **yearly sequence**, which processes each **tick**:

  1. Rainfall
  2. Green biomass growth
  3. Livestock reproduction
  4. Households relocating herds, according to their *behavioral type*
  5. Destocking, feeding and consumption of biomass
  7. Reserve biomass growth

# A detailed description of the model and its processes can be found in the ODD+D protocol

## Things to try

### Influence of demographic settings

Varying the initial *number-households* in the system.

### Influence of behavioral change

Comparing homogeneous with heterogeneous groups of households (*homog-behav-types?* true or false) and varying the share of each *behavioral type* (*number-TRAD*, *number-MAX*, *number-SAT*).

## REFERENCES

Dressler, G., B. Müller, and K. Frank (2012): _**Mobility – a panacea for pastoralism? An ecological-economic modelling approach.**_, *Proceedings of the iEMSs Fifth Biennial Meeting: International Congress on Environmental Modelling and Software (iEMSs 2012)*. International Environmental Modelling and Software Society, Leipzig, Germany, July 2012.

Martin, R., B. Müller, A. Linstädter, and K. Frank (2014): _**How much climate change can pastoral livelihoods tolerate? Modelling rangeland use and evaluating risk.**_, *Global Environmental Change*, 24, 183-192.

Müller, B., K. Frank and C. Wissel (2007): _**Relevance of rest periods in non-equilibirum rangeland systems - a modelling analysis**_, *Agricultural Systems*, 92, 295–317.

Schulze, J. (2011): _**Risk Measures and Decision Criteria for the Management of Natural Resources Under Uncertainty - Application to an Ecological-Economic Grazing Model**_, *Master Thesis*, Helmholtz Centre for Environmental Research & Ernst- Moritz-Arndt-University of Greifswald.
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

house2
false
0
Rectangle -7500403 true true 15 120 285 285
Polygon -7500403 true true 15 120 150 15 285 120

house3
false
0
Rectangle -7500403 true true 15 75 195 180
Polygon -7500403 true true 15 75 105 15 195 75
Polygon -7500403 true true 120 285 120 255 105 240 105 210 120 195 195 195 210 210 225 195 240 195 270 210 270 225 255 240 240 240 210 255 210 285 195 285 195 255 135 255 135 285
Polygon -7500403 true true 105 210 90 270 105 240
Polygon -7500403 true true 195 285 180 210 225 210 210 285
Polygon -7500403 true true 135 285 150 210 120 210 105 240
Polygon -7500403 true true 240 195 210 180 225 195
Polygon -7500403 true true 240 195 255 195 255 210

house4
false
0
Rectangle -7500403 true true 15 75 165 195
Polygon -7500403 true true 15 75 90 15 165 75
Circle -7500403 true true 208 88 62
Polygon -7500403 true true 195 150 285 150 270 225 210 225
Polygon -7500403 true true 210 225 195 300 225 300 240 255 255 300 285 300 270 225
Polygon -7500403 true true 195 150 165 225 180 225 210 180
Polygon -7500403 true true 285 150 300 180 300 225 255 165 285 150

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

none
false
0

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

sheep-gone
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83
Polygon -2674135 true false 0 15 15 0 300 285 285 300 0 15
Polygon -2674135 true false 0 285 15 300 300 15 285 0

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.1.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="full_sensit_analysis_TRAD" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="10" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;TRAD&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <steppedValueSet variable="intrinsic-preference" first="0" step="0.05" last="1"/>
    <steppedValueSet variable="social-susceptibility" first="0" step="0.05" last="1"/>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0.2"/>
      <value value="0.4"/>
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="full_sensit_analysis_SAT" repetitions="100" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="10" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="10" step="5" last="150"/>
    <steppedValueSet variable="satisficing-trials" first="5" step="5" last="100"/>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="full_sensit_analysis" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <enumeratedValueSet variable="number-households">
      <value value="50"/>
      <value value="80"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="timesteps">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <steppedValueSet variable="intrinsic-preference" first="0" step="0.05" last="1"/>
    <steppedValueSet variable="social-susceptibility" first="0" step="0.05" last="1"/>
    <steppedValueSet variable="satisficing-threshold" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
      <value value="20"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0.2"/>
      <value value="0.4"/>
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="full_sensit_analysis2" repetitions="50" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <enumeratedValueSet variable="number-households">
      <value value="50"/>
      <value value="80"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="timesteps">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <steppedValueSet variable="intrinsic-preference" first="0" step="0.05" last="1"/>
    <steppedValueSet variable="social-susceptibility" first="0" step="0.05" last="1"/>
    <steppedValueSet variable="satisficing-threshold" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
      <value value="20"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0.2"/>
      <value value="0.4"/>
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_satThres_satTrials_nH_1-20_gr1_05_mr_01" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="1" step="1" last="20"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="1" step="1" last="100"/>
    <steppedValueSet variable="satisficing-trials" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_nH_gr1_0-1_mr_00-02" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <steppedValueSet variable="gr1" first="0" step="0.25" last="1"/>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0"/>
      <value value="0.1"/>
      <value value="0.2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_satThres_satTrials_nH_21-40_gr1_05_mr_01" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="21" step="1" last="40"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="1" step="1" last="100"/>
    <steppedValueSet variable="satisficing-trials" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_satThres_satTrials_nH_41-60_gr1_05_mr_01" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="41" step="1" last="60"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="1" step="1" last="100"/>
    <steppedValueSet variable="satisficing-trials" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_satThres_satTrials_nH_61-80_gr1_05_mr_01" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="61" step="1" last="80"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="1" step="1" last="100"/>
    <steppedValueSet variable="satisficing-trials" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_satThres_satTrials_nH_81-100_gr1_05_mr_01" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="81" step="1" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="1" step="1" last="100"/>
    <steppedValueSet variable="satisficing-trials" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_TRAD_nH_everyStep" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;TRAD&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0.95"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0.2"/>
      <value value="0.4"/>
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_nH_everyStep" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_nH_everyStep" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="1" step="1" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="50"/>
      <value value="60"/>
      <value value="70"/>
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
      <value value="20"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_theorOpt_exitRule_MAX_everyStep" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <enumeratedValueSet variable="number-households">
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.1"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="mr" first="0.1" step="0.01" last="0.15"/>
    <enumeratedValueSet variable="R0part">
      <value value="0.5"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_theorOpt_exitRule_SAT_everyStep" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-coef-surviving</metric>
    <metric>descriptive-norm</metric>
    <enumeratedValueSet variable="number-households">
      <value value="70"/>
      <value value="80"/>
      <value value="90"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.1"/>
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="mr" first="0.1" step="0.01" last="0.15"/>
    <enumeratedValueSet variable="R0part">
      <value value="0.5"/>
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="30" step="5" last="60"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="5"/>
      <value value="10"/>
      <value value="20"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_fullParamVar_yearly" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_TRAD_fullParamVar" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>repeat 10 [ go ]</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;TRAD&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <steppedValueSet variable="intrinsic-preference" first="0" step="0.1" last="1"/>
    <steppedValueSet variable="social-susceptibility" first="0" step="0.1" last="1"/>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="true"/>
    </enumeratedValueSet>
    <steppedValueSet variable="resting-threshold" first="0" step="0.2" last="0.6"/>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_fullParamVar_seasons" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="800"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="20" step="10" last="100"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_smallParamVar" repetitions="100" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <enumeratedValueSet variable="number-households">
      <value value="50"/>
      <value value="70"/>
      <value value="90"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.13"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_smalllParamVar" repetitions="100" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <enumeratedValueSet variable="number-households">
      <value value="50"/>
      <value value="70"/>
      <value value="90"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.13"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="30"/>
      <value value="35"/>
      <value value="40"/>
      <value value="45"/>
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="5"/>
      <value value="10"/>
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_smalllParamVar2" repetitions="25" runMetricsEveryStep="false">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.125"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="60"/>
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_fullParamVar_monthly" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="2400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="20" step="10" last="100"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_fullParamVar_yearly" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="20" step="10" last="100"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_fullParamVar_seasonal" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="800"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="4"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_fullParamVar_monthly" repetitions="10" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print behaviorspace-run-number</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="timesteps">
      <value value="2400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_MAX_ticksPerYear_1_12" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print (word behaviorspace-run-number ": ticks = " ticks )</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="years">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="1"/>
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;MAX&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
  <experiment name="RAFI_SAT_ticksPerYear_1_12" repetitions="25" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <final>print (word behaviorspace-run-number ": ticks = " ticks )</final>
    <metric>ticks</metric>
    <metric>surviving-households</metric>
    <metric>mean-reserve-biomass</metric>
    <metric>mean-green-biomass</metric>
    <metric>mean-livestock</metric>
    <metric>total-livestock</metric>
    <metric>mean-destock</metric>
    <metric>total-destock</metric>
    <metric>sum-livestock-end</metric>
    <metric>gini-livestock</metric>
    <metric>gini-destock</metric>
    <metric>descriptive-norm</metric>
    <steppedValueSet variable="number-households" first="50" step="5" last="100"/>
    <enumeratedValueSet variable="years">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="ticks-per-year">
      <value value="1"/>
      <value value="12"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seasonal-destock?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="homog-behav-types?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-TRAD">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-SAT">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-MAX">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="number-random">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="behavioral-type">
      <value value="&quot;SAT&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-exit?">
      <value value="true"/>
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr2">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="b">
      <value value="0.8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mg">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="mr">
      <value value="0.1"/>
      <value value="0.125"/>
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="R0part">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="intrinsic-preference">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="social-susceptibility">
      <value value="0"/>
    </enumeratedValueSet>
    <steppedValueSet variable="satisficing-threshold" first="20" step="10" last="100"/>
    <enumeratedValueSet variable="satisficing-trials">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="knowledge-radius">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="resting-threshold">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="start-on-homepatch?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="200"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-std">
      <value value="100"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rain?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
